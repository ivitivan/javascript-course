### Введение во фронтенд

---

### Начнем с простого: найдем сумму двух чисел

```js
const a = 1
const b = 2
const c = a + b

console.log('c = ' + c) // здесь плюс - это конкатенация
```

Переменные также можно объявлять через `var` и `let`:
```js
var str = 'Какая-та строка'
let isColdToday = true
```

---

### Напишем функцию для сложения:

```js
function sum(a, b) {
  return a + b
}

const c = sum(1, 2)

console.log(`c = ${c}`) /* здесь уже шаблонные строки */
```

Комментарии в коде:

```js
// Однострочный комментарий
/* Много
   строчный
   комментарий
*/
```

---

### Добавим интерфейс:

```html
<div id="sum">
  <input type="text" name="a"/>
  <input type="text" name="b"/>
  <button id="sum-button" type="button">Sum</button>
  <div id="result"></div>
</div>
```

Для идентификации элемента, можно использовать `class`:
```html
<div class="calculator"></div>
```

---

### Хендлим (handle) инпут

```html
<div id="sum">
  <input type="text" name="a"/>
  <input type="text" name="b"/>
  <button id="sum-button" type="button">Sum</button>
  <div id="result"></div>
</div>
```

```js
const aDom = document.querySelector('[name="a"]')

aDom.addEventListener('input', (event) => {
  state.a = Number.parseFloat(event.target.value)
})
```

Выбираем элемент с атрибутом name, равный "a"
```js
document.querySelector('[name="a"]')
```

Краткая запись фукнции:

```js
const helloWorld = () => console.log('Hello, World!')
```

---

### Второй инпут

```html
<div id="sum">
  <input type="text" name="a"/>
  <input type="text" name="b"/>
  <button id="sum-button" type="button">Sum</button>
  <div id="result"></div>
</div>
```

```js
const bDom = document.querySelector('[name="b"]')

bDom.addEventListener('input', (event) => {
  state.b = Number.parseFloat(event.target.value)
})
```

---

### Хендлим клик по кнопке

```html
<div id="sum">
  <input type="text" name="a"/>
  <input type="text" name="b"/>
  <button id="sum-button" type="button">Sum</button>
  <div id="result"></div>
</div>
```

```js
const sumButton = document.querySelector('#sum-button')

sumButton.addEventListener('click', (event) => {
  const result = sum(state.a, state.b)

  resultDom.innerHTML = result
})
```

Выбираем элемент с id, равный sum-button:

```js
document.querySelector('#sum-button')
```

---

### Выведем результат

```js
const state = {
  a: undefined,
  b: undefined,
}

function sum(a, b) {
  return a + b
}

const resultDom = document.querySelector('#result')

const sumButton = document.querySelector('#sum-button')

sumButton.addEventListener('click', (event) => {
  const result = sum(state.a, state.b)

  resultDom.innerHTML = result
})
```

---

### Немного рефакторинга:

```js
const state = {
  a: undefined,
  b: undefined,
}

function sum(a, b) {
  return a + b
}

function initInput(name, state) {
  const input = document.querySelector(`[name="${name}"]`)

  input.addEventListener('input', (event) => {
    state[name] = Number.parseFloat(event.target.value)
  })

  return input
}

const aDom = initInput('a', state)
const bDom = initInput('b', state)
const resultDom = document.querySelector('#result')
const sumButton = document.querySelector('#sum-button')

sumButton.addEventListener('click', (event) => {
  const result = sum(state.a, state.b)

  resultDom.innerHTML = result
})
```

---

### Напишем небольшой фреймворк для фронта:

Что хотим:

- Поменялся стейт -> отрисуем новый HTML

- Функция для создания компонентов

---

### Стейт

```js
function stateCreator(initialState) {
  const state = initialState
  const callbacks = {}

  return {
    get: () => state,
    update: (nextState) => {
      Object.assign(state, nextState)

      for (let callbackName in callbacks) {
        callbacks[callbackName].forEach((callback) => callback(state))
      }
    },
    on: (callbackName, callback) => {
      if (!callbacks[callbackName]) {
        callbacks[callbackName] = []
      }

      callbacks[callbackName].push(callback)
    },
  }
}
```

---

### Компонент

```js
function component(props) {
  const el = document.createElement(props.tag)

  if (props.onInput) {
    el.addEventListener('input', props.onInput)
  }

  return el
}
```

---

### Все вместе

```html
<div id="root"></div>

<script>
function main() {
  const stateHolder = stateCreator({
    a: undefined,
    b: undefined,
  })

  stateHolder.on('change', (state) => {
    const dom = component({
      tag: 'div',
      id: 'sum',
      children: [
        component({
          tag: 'input',
          type: 'text',
          name: 'a',
          value: state.a,
          onInput: (event) => stateHolder.update({
            a: Number.parseFloat(event.target.value),
            b: state.b,
          }),
        }),
        component({
          tag: 'input',
          type: 'text',
          name: 'a',
          value: state.b,
          onInput: (event) => stateHolder.update({
            a: state.a,
            b: Number.parseFloat(event.target.value),
          }),
        }),
        component({
          tag: 'button',
          id: 'sum-button',
          type: 'button',
          children: 'Sum',
          onClick: (event) => stateHolder.update({
            a: state.a,
            b: state.b,
            sum: sum(state.a, state.b),
          })
        }),
        component({
          tag: 'div',
          id: 'result',
          children: state.sum,
        }),
      ],
    })

    render(dom, '#root')
  })

  stateHolder.update({
    a: undefined,
    b: undefined,
  })
}

main()

function component(props) {
  const el = document.createElement(props.tag)

  if (props.type) {el.type = props.type}
  if (props.name) {el.name = props.name}
  if (props.id) {el.id = props.id}
  if (props.value) {el.value = props.value}

  if (props.children && (props.children instanceof Array)) {
    props.children.forEach((i) => el.append(i))
  }

  if (props.children && (typeof props.children === 'string' || typeof props.children === 'number')) {
    el.textContent = props.children
  }

  if (props.onInput) {
    el.addEventListener('input', props.onInput)
  }

  if (props.onClick) {
    el.addEventListener('click', props.onClick)
  }

  return el
}

function render(dom, mountPoint) {
  const mount = document.querySelector(mountPoint)

  mount.innerHTML = ''
  mount.append(dom)
}

function stateCreator(initialState) {
  const state = initialState
  const callbacks = {}

  return {
    get: () => state,
    update: (nextState) => {
      Object.assign(state, nextState)

      for (let callbackName in callbacks) {
        callbacks[callbackName].forEach((callback) => callback(state))
      }
    },
    on: (callbackName, callback) => {
      if (!callbacks[callbackName]) {
        callbacks[callbackName] = []
      }

      callbacks[callbackName].push(callback)
    },
  }
}

function sum(a, b) {
  return a + b
}
</script>
```
