### Введение во фронтенд

---

### Начнем с простого: найдем сумму двух чисел

```js
const a = 1
const b = 2
const c = a + b

console.log('c = ' + c) // здесь плюс - это конкатенация
```

Переменные также можно объявлять через `var` и `let`:
```js
var str = 'Какая-та строка'
let isColdToday = true
```

---

### Напишем функцию для сложения:

```js
function sum(a, b) {
  return a + b
}

const c = sum(1, 2)

console.log(`c = ${c}`) /* здесь уже шаблонные строки */
```

Комментарии в коде:

```js
// Однострочный комментарий
/* Много
   строчный
   комментарий
*/
```

---

### Добавим интерфейс:

```html
<div id="sum">
  <input type="text" name="a"/>
  <input type="text" name="b"/>
  <button id="sum-button" type="button">Sum</button>
  <div id="result"></div>
</div>
```

Для идентификации элемента, можно использовать `class`:
```html
<div class="calculator"></div>
```

---

### Хендлим (handle) инпут

```html
<div id="sum">
  <input type="text" name="a"/>
  <input type="text" name="b"/>
  <button id="sum-button" type="button">Sum</button>
  <div id="result"></div>
</div>
```

```js
const aDom = document.querySelector('[name="a"]')

aDom.addEventListener('input', (event) => {
  state.a = Number.parseFloat(event.target.value)
})
```

Выбираем элемент с атрибутом name, равный "a"
```js
document.querySelector('[name="a"]')
```

Краткая запись фукнции:

```js
const helloWorld = () => console.log('Hello, World!')
```

---

### Второй инпут

```html
<div id="sum">
  <input type="text" name="a"/>
  <input type="text" name="b"/>
  <button id="sum-button" type="button">Sum</button>
  <div id="result"></div>
</div>
```

```js
const bDom = document.querySelector('[name="b"]')

bDom.addEventListener('input', (event) => {
  state.b = Number.parseFloat(event.target.value)
})
```

---

### Хендлим клик по кнопке

```html
<div id="sum">
  <input type="text" name="a"/>
  <input type="text" name="b"/>
  <button id="sum-button" type="button">Sum</button>
  <div id="result"></div>
</div>
```

```js
const sumButton = document.querySelector('#sum-button')

sumButton.addEventListener('click', (event) => {
  const result = sum(state.a, state.b)

  resultDom.innerHTML = result
})
```

Выбираем элемент с id, равный sum-button:

```js
document.querySelector('#sum-button')
```

---

### Выведем результат

```js
const state = {
  a: undefined,
  b: undefined,
}

function sum(a, b) {
  return a + b
}

const resultDom = document.querySelector('#result')

const sumButton = document.querySelector('#sum-button')

sumButton.addEventListener('click', (event) => {
  const result = sum(state.a, state.b)

  resultDom.innerHTML = result
})
```

---

### Немного рефакторинга:

```js
const state = {
  a: undefined,
  b: undefined,
}

function sum(a, b) {
  return a + b
}

function initInput(name, state) {
  const input = document.querySelector(`[name="${name}"]`)

  input.addEventListener('input', (event) => {
    state[name] = Number.parseFloat(event.target.value)
  })

  return input
}

const aDom = initInput('a', state)
const bDom = initInput('b', state)
const resultDom = document.querySelector('#result')
const sumButton = document.querySelector('#sum-button')

sumButton.addEventListener('click', (event) => {
  const result = sum(state.a, state.b)

  resultDom.innerHTML = result
})
```
